(function ($) {
    var dataScroll = $('[data-scroll="off"]');
    $(window).on('load', function () {
        var top = $(window).scrollTop() + ($(window).height() * 0.8);
        for (var i = 0; i < dataScroll.length; i++) {
            if ($(dataScroll[i]).offset().top < top && $(dataScroll[i]).attr('data-scroll') == 'off') {
                $(dataScroll[i]).attr('data-scroll', "on");
            }
        }
        $(window).on('scroll', function () {
            var top = $(window).scrollTop() + ($(window).height() * 0.8);
            for (var i = 0; i < dataScroll.length; i++) {
                if ($(dataScroll[i]).offset().top < top && $(dataScroll[i]).attr('data-scroll') == 'off') {
                    $(dataScroll[i]).attr('data-scroll', "on");
                    dataScroll = $('[data-scroll="off"]');
                }
            }
        });
    });

    $('.slider').slick({
        infinite: true,
        speed: 700,
        slidesToShow: 1
    });

    //POP-UP
    var popUp = $('.popup-tel');
    var popUpForm = $('.popup-tel form');
    $('[data-popup]').click(function () {
        var popUpFormHeight = popUpForm.height();
        var top = $(window).scrollTop();
        var height = $(window).height();
        popUpForm.css({
            // 'left': 'calc(50% - '+popUpFormWigth*0.5+'px)',
            '-webkit-transform': "translateY(" + top + "px)",
            '-moz-transform': "translateY(" + top + "px)",
            '-ms-transform': "translateY(" + top + "px)",
            '-o-transform': "translateY(" + top + "px)",
            'transform': "translateY(" + top + "px)"
        });
        if (height > popUpFormHeight) {
            $(window).scroll(function () {
                top = $(window).scrollTop();
                popUpForm.css({
                    '-webkit-transform': "translateY(" + top + "px)",
                    '-moz-transform': "translateY(" + top + "px)",
                    '-ms-transform': "translateY(" + top + "px)",
                    '-o-transform': "translateY(" + top + "px)",
                    'transform': "translateY(" + top + "px)"
                });
            });
        }
        console.log(popUpFormHeight, top, height);
        popUp.addClass('open');
        popUp.click(function () {
            if ($(event.target).hasClass('popup-tel')) {
                popUp.removeClass('open');
            }
            $('.close').click(function () {
                popUp.removeClass('open');
            })
        })
    });

    //VALID
    var inp = {
        input:$('input'),
        iName:"",
        vName: /^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z\s]{1,500}$/,
        vEmail: /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/,
        vTell: /^((\d|\+\d)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6,13}$/,
        this:"",
        valName:"",
        valEmail:"",
        valTell:"",
        formBtn: $('form .btn'),
        formInput: "",
        thisForm:"",
        boolName: false,
        boolEmail: false,
        text:""
    };
    inp.input.blur(function () {
        inp.this = $(this);
        inp.iName = $(this).attr('name');
        if("name" == inp.iName){
            if(inp.vName.test(inp.this.val())){
                inp.this.removeClass('error');
                inp.valName = inp.this.val();
                inp.boolName = true;
            } else {
                inp.this.addClass('error');
                inp.boolName = false;
            }
        }else if ("email" == inp.iName){
            if(inp.vEmail.test(inp.this.val()) || inp.vTell.test(inp.this.val())){
                inp.this.removeClass('error');
                inp.valEmail = inp.this.val();
                inp.boolEmail = true;
            } else {
                inp.this.addClass('error');
                inp.boolEmail = false;
            }
        }
    });
    inp.formBtn.click(function () {
        inp.thisForm = $(this).closest('form');
        inp.formInput = $(this).closest('form').find('input');
        inp.text = $(this).closest('form').find('textarea').val();
        inp.formInput.each( function( i, el ){
            inp.this = $(el);
            inp.iName = $(el).attr('name');
            if("name" == inp.iName){
                if(inp.vName.test(inp.this.val())){
                    inp.this.removeClass('error');
                    inp.valName = inp.this.val();
                    inp.boolName = true;
                } else {
                    inp.this.addClass('error');
                    inp.boolName = false;
                }
            }else if ("email" == inp.iName){
                if(inp.vEmail.test(inp.this.val()) || inp.vTell.test(inp.this.val())){
                    inp.this.removeClass('error');
                    inp.valEmail = inp.this.val();
                    inp.boolEmail = true;
                } else {
                    inp.this.addClass('error');
                    inp.boolEmail = false;
                }
            }
        } );
        if(inp.valName&&inp.boolEmail){
            var data1 = {name: inp.valEmail, tell: inp.valEmail ,massage : inp.text};
            $.ajax({
                // url: "../sender.php",
                type: "POST",
                data: data1,
                success: function () {
                   inp.thisForm.addClass('good');
                   console.log(data1);
                }
            });
        }

    });
})(jQuery);
